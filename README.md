# Content Aware Image Scaling AWS Lambda

Input: See `event.json`

Output: Link to JPEG image

**Caire is open source library available [here](https://github.com/esimov/caire)**

## What was done on AWS Lambda

* Lambda
    * deploy via python lambda lib
* API Gateway
    * default values
* S3
    * added policy to allow everyone to view bucket contents
* ~~SNS~~
    * ~~handle callbacks to frontend with link~~

### Pusher

Since SNS is kinda retarded, I've switched this functionality to Pusher.
This also resolves privacy issues, as I can simply have channel per request.


## Lessons learned

* AWS Lambda is very restricted when it comes to files. Put everything into `/tmp` folder
* New regions support only new signing for s3
* Currently python runtime for lambda does not allow binary responses
* API Gateway has hard timeout limit of 30 seconds!
    * Due to this I had to redesign application to be asynchronous
    * Turns out that mere frontend cannot subscribe to SNS topic
        * Switched this functionality to Pusher
* Lambda reuses containers. If you left something in `/tmp` folder
then it might get reused. Clean up after yourself!
* When max memory is reached, lambda does not crash - it simply
disallows further allocation. Max memory limit is 3008MB.
